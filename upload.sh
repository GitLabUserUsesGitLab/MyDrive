#!/usr/bin/bash

mkdir upload && cd upload
wget $UPLOAD_URL
for i in `find -type f -name "*\?*"`; do mv $i `echo $i | cut -d? -f1`; done
